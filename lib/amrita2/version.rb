module Amrita2
  module Version
    MAJOR=2
    MINOR=0
    TINY=2

    STRING=[MAJOR,MINOR,TINY].join('.')
  end
end
