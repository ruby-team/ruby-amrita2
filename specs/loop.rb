
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2
include Amrita2::Filters

context "LOOP" do

  specify "simple array" do
    @t = Amrita2::Template.new <<EOF
<html>
<head>
<title am:src='title' />
</head>
<body>
<p class='text' am:src='text' />
<ul>
  <li am:src='loop' />
</ul>
</body>
</html>
EOF
    data = {
      :title=>'Amrita2', 
      :text=>'Hello, Amrita2',
      :loop=>[1,2,3]
    }
    @t.test_with(data) do |result|
      result.should_be_samexml_as <<-EOF
      <html>
      <head>
        <title>Amrita2</title>
      </head>
      <body>
        <p class = "text">Hello, Amrita2</p>
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
        </ul>
      </body>
      </html>
      EOF
    end
  end
  
  specify "Each filter" do
    t = Amrita2::Template.new <<-EOF
    <ul>
      <<li:list|ToHash[:body]|Each[:class=>['oddline', 'evenline']]|Attr[:class, :body]>>
    </ul>
    EOF

    t.test_with(:list=>1..3) do |result|
      result.should_be_samexml_as <<-EOF
       <ul>
         <li class='oddline'>1</li>
         <li class='evenline'>2</li>
         <li class='oddline'>3</li>
       </ul>
      EOF
    end
    
    t = Amrita2::Template.new <<-EOF
    <ul>
      <<li:list|Each[:class=>[:odd, :even]]|Attr[:class, :body]>>
    </ul>
    EOF

    list = (1..3).collect do |n|
      { :body => n, :odd=>'oddline', :even=>'evenline' }
    end
    
    t.test_with(:list=>list) do |result|
      result.should_be_samexml_as <<-EOF
       <ul>
         <li class='oddline'>1</li>
         <li class='evenline'>2</li>
         <li class='oddline'>3</li>
       </ul>
      EOF
    end
  end

  specify "table" do
    @t = Amrita2::Template.new <<EOF
<html>
<head>
<title am:src='title' />
</head>
<body>
<table>
  <tr am:src='table'>
     <td am:src='aaa' />
     <td am:src='bbb' />
  </tr>
</table>
</body>
</html>
EOF
    data = {
      :title=>'Amrita2', 
      :text=>'Hello, Amrita2',
      :table=>[
        { :aaa=>'11', :bbb=>'12' },
        { :aaa=>'21', :bbb=>'22' },
        { :aaa=>'31', :bbb=>'32' },
      ]
    }
    @t.test_with(data) do |result|
      result.should_be_samexml_as <<-EOF
      <html>
      <head>
        <title>Amrita2</title>
      </head>
      <body>
       <table>
         <tr>
            <td>11</td>
            <td>12</td>
         </tr>
         <tr>
            <td>21</td>
            <td>22</td>
         </tr>
         <tr>
            <td>31</td>
            <td>32</td>
          </tr>
        </table>
       </body>
      </html>
      EOF
    end
  end

  specify "array of struct" do
    @t = Amrita2::Template.new <<EOF
<table border='1'>
  <tr><th>name</th><th>author</th><th>website</th></tr>
  <tr am:src='table1'>
    <td am:src='name'></td>
    <td am:src='author'></td>
    <td><a  am:src='website|Attr[:href, :body]' /> </td>
  </tr>
</table>
EOF
    Lang1 = Struct.new(:name, :author, :site)
    WebSite1 = Struct.new(:name, :url)
    Lang1.module_eval do
      def website
        {
          :href=>site.url,
          :body=>site.name
        }
      end
    end

    data = {
      :table1 => [ 
        Lang1.new("Ruby", "matz", WebSite1.new('Ruby Home Page', 'http://www.ruby-lang.org/')),
        Lang1.new("Perl", "Larry Wall", WebSite1.new('The Source for Perl', 'http://perl.com/')),
        Lang1.new("Python","Guido van Rossum", WebSite1.new('Python Programing Language', 'http://www.python.org/'))
      ]
    }
    @t.test_with(data) do |result|
      result.should_be_samexml_as <<-EOF
        
        <table border='1'>
        <tr>
        <th>name</th>
        <th>author</th>
        <th>website</th>
        </tr>
        <tr>
        <td>Ruby</td>
        <td>matz</td>
        <td>
        <a href='http://www.ruby-lang.org/'>Ruby Home Page</a>
        </td>
        </tr>
        <tr>
        <td>Perl</td>
        <td>Larry Wall</td>
        <td>
        <a href='http://perl.com/'>The Source for Perl</a>
        </td>
        </tr>
        <tr>
        <td>Python</td>
        <td>Guido van Rossum</td>
        <td>
        <a href='http://www.python.org/'>Python Programing Language</a>
        </td>
        </tr>
        </table>
       EOF
    end
  end
end

