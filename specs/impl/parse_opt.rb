
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2
include Amrita2::Util

context "parse_opt" do
  include OptionSupport
  setup do
    doc = REXML::Document.new "<div class='aaa'><a href='bbb' title='ccc'>ddd</a></div>"
    @e = doc.root
  end
  
  specify "give simbols" do
    parse_opt().should == {}
    parse_opt(:aaa).should == {:aaa=>:aaa}
    parse_opt(:aaa, :bbb).should == {:aaa=>:aaa, :bbb=>:bbb}
  end
  
  specify "give hash" do
    parse_opt(:aaa=>:aaa).should == {:aaa=>:aaa}
    parse_opt(:aaa=>"aaa").should == {:aaa=>"aaa"}
    parse_opt(:aaa=>"aaa", :bbb=>:bbb).should == {:aaa=>"aaa", :bbb=>:bbb}
  end
  
  specify "give hash and simbol" do
    parse_opt(:aaa, :bbb=>:bbb).should == {:aaa=>:aaa, :bbb=>:bbb}
    parse_opt(:aaa, :bbb=>"bbb").should == {:aaa=>:aaa, :bbb=>"bbb" }
    parse_opt(:xxx, :aaa=>"aaa", :bbb=>:bbb).should == {:aaa=>"aaa", :bbb=>:bbb, :xxx=>:xxx}
  end
end

