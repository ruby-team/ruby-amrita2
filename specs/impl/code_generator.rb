
require 'amrita2'

context "" do
  include Amrita2::Runtime
  setup do
    @g = Amrita2::Core::CodeGenerator.new
  end

  def cg_result
    __binding__ = binding
    eval @g.result
  end

  def cg_result_for_class(value)
    cls = Class.new
    cls.module_eval @g.result
    cls.new.render_with(value)
  end

  specify "code" do
    @g.code "1"
    cg_result.should == 1
  end

  specify "put_constant" do
    @g.with_stream do
      @g.put_constant 1
    end
    cg_result.should == "1"
  end

  specify "if true" do
    @g.with_stream do
      @g.if_("true") do
        @g.put_constant 2
      end
    end
    cg_result.should == "2"
  end

  specify "if false " do
    @g.with_stream do
      @g.if_("false") do
        @g.put_constant 2
      end
    end
    cg_result.should == ""
  end

  specify "case " do
    @g.with_stream do
      @g.case_("1") do
        @g.when_("String") do
          @g.put_constant 1
        end
        @g.when_("Integer") do
          @g.put_constant 2
        end
      end
    end
    cg_result.should == "2"
  end

  specify "def method" do
    @g.def_method("render_with", "value") do
      @g.put_constant("<")
      @g.code("value +=1")
      @g.put_expression("(value * 2).to_s")
      @g.put_constant(">")
    end
    cg_result_for_class(3).should == "<8>"
  end
  
  specify "multi stream" do
    @g.with_stream do
      @g.put_constant("abc")
      @g.with_stream(:aaa) do 
        @g.put_constant("123")
        @g.put_constant("456")
      end
      @g.put_constant("def")
      @g.put_stream(:aaa)
    end
    cg_result.should == "abcdef123456"
  end
end
