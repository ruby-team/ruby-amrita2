require 'amrita2'
require 'amrita2/testsupport'

context "amxml parser" do
  include Amrita2

  it "tag only" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[a])
    a.tag.should == "a"
    a.attr.should == ""
    a.should_not be_has_amxml_attrs
  end

  specify "tag with namespace only" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[xx:a/])
    a.tag.should == "xx:a"
    a.attr.should == ""
    a.should_not be_has_amxml_attrs

    a = amxml.parse(%[xx:a])
    a.tag.should == "xx"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == "a"

    a = amxml.parse(%[xx:a b="c"])
    a.tag.should == "xx:a"
    a.attr.should == ' b="c"'
    a.should_not be_has_amxml_attrs
  end

  specify "tag and attrs" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[a href="http://amrita2.rubyforge.org/"])
    a.tag.should == "a"
    a.attr.should == %[ href="http://amrita2.rubyforge.org/"]
    a.should_not be_has_amxml_attrs
    a = amxml.parse(%[a href='http://amrita2.rubyforge.org/'])
    a.tag.should == "a"
    a.attr.should == %[ href='http://amrita2.rubyforge.org/']
    a.should_not be_has_amxml_attrs

    a = amxml.parse(%[a href='http://amrita2.rubyforge.org/' class="xxx"])
    a.tag.should == "a"
    a.attr.should == %[ href='http://amrita2.rubyforge.org/' class="xxx"]
    a.should_not be_has_amxml_attrs

    a = amxml.parse(%[a   href = 'http://amrita2.rubyforge.org/' class =   "xxx"])
    a.tag.should == "a"
    a.attr.should == %[   href = 'http://amrita2.rubyforge.org/' class =   "xxx"]
    a.should_not be_has_amxml_attrs

    a = amxml.parse(%[x:a y:href='http://amrita2.rubyforge.org/' zz:class="xxx"])
    a.tag.should == "x:a"
    a.attr.should == %[ y:href='http://amrita2.rubyforge.org/' zz:class="xxx"]
    a.should_not be_has_amxml_attrs
  end

  specify "src" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[:xxx])
    a.tag.should == "_"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == "xxx"

    a = amxml.parse(%[a:xxx])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == "xxx"
  end

  specify "src and filters" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[a:xxx|Attr])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == "xxx"
    a.filters.should == "Attr"

    a = amxml.parse(%[a:xxx| \n Attr])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == "xxx"
    a.filters.should == "Attr"
  end

  specify "loop" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[ [1..5] ])
    a.tag.should == "_"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == nil
    a.filters.should == nil
    a.for.should == %[1..5]

    a = amxml.parse(%[a [1..5] ])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == nil
    a.filters.should == nil
    a.for.should == %[1..5]
  end

  specify "cond" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    '?![a == 1]'.should match(%r[#{ amxml::S_AMVARS}])
    a = amxml.parse(%[a ?![a == 1] ])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == nil
    a.filters.should == nil
    a.skipif.should == "a == 1"

    a = amxml.parse(%[a  ?[ b == 1 ]  ])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == nil
    a.filters.should == nil
    a.skipif.should == nil
    a.skipunless.should == "b == 1"
  end

  specify "value" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    a = amxml.parse(%[a { :xxx=>1 }])
    a.tag.should == "a"
    a.attr.should == ""
    a.should be_has_amxml_attrs
    a.src_name.should == nil
    a.filters.should == nil
    a.value.should == ":xxx=>1"
  end

  specify "result" do
    amxml = Amrita2::Core::PreProcessor::AmXml
    amxml.parse(%[a { :xxx=>1 }]).result(true).should == %[<a am:v='HashDelegator.new($_) { {:xxx=>1} }' />]
    amxml.parse(%[a href="xxx" { :xxx=>1 }]).result(true).should == %[<a am:v='HashDelegator.new($_) { {:xxx=>1} }' href="xxx" />]
    amxml.parse(%[a ?[a == 1]]).result(true).should == %[<a am:skipif='not(a == 1)' />]
    amxml.parse(%[a:xxx|Attr]).result(true).should == %[<a am:src='xxx|Attr' />]
    amxml.parse(%[tr:clients|NVar[:surname, :firstname, :email]]).result(false).should ==
       %[<tr am:src='clients|NVar[:surname, :firstname, :email]' >]
    amxml.parse(%[ [1..5] ]).result(false).should ==
      %[<_ am:for='1..5' >]
    amxml.parse("tr:clients  \n          |NVar[:surname, :firstname, :email]").result(false).should ==
      "<tr am:src='clients|NVar[:surname, :firstname, :email]' >"
  end
end

context "preprocessor" do
  include Amrita2
  specify "check CDATA regexp" do
    t = "aaa<![CDATA[bbb"
    t =~ %r[(.*?)(<!\[CDATA\[)(.*)]
    $1.should == "aaa"
    $2.should == "<![CDATA["
    $3.should == "bbb"
    (t+t) =~ %r[(.*?)(<!\[CDATA\[)(.*)]
    $1.should == "aaa"
    $2.should == "<![CDATA["
    $3.should == "bbb" + t
    $3 =~ %r[(.*?)(<!\[CDATA\[)(.*)]
    $1.should == "bbbaaa"
    $2.should == "<![CDATA["
    $3.should == "bbb"
  end

  specify "check regexp"do
    tag = %r[\w*]
    attr = %r[(?:\s*\w+=(?:".*?"|'.*?'))*] #"
    src_start = %r[(?::| \[|\?\[|\{)]
    r = %r[^\s*<<\s*(#{tag})(#{attr})(#{src_start}(.*?))?<\s*$]
    "<<<".should match(r)
    "<<p<".should match(r)
    "<<p:title<".should match(r)
    "<<p:title [1..5]<".should match(r)
    "<<p:title ?[$_ == 1]<".should match(r)
    "<<p:title { :val => 2}<".should match(r)
    "<<:title<".should match(r)
    "<<:title [1..5]<".should match(r)
    "<<:title ?[$_ == 1]<".should match(r)
    "<<:title { :val => 2}<".should match(r)
    "<< [1..5]<".should match(r)
    "<< ?[$_ == 1]<".should match(%r[(<< #{src_start}(.*?)?<$)])
    "<< ?[$_ == 1]<".should match(r)
    "<<{ :val => 2}<".should match(r)
    " <<li< ".should match(r)
  end

  specify "preprocessor" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("aaa<![")
    ret.should == "aaa<!["
    proc {p.process("aaa<![CDATA[")}.should raise_error(RuntimeError)
    ret = p.process("aaa<![CDATA[bbb]]>ccc")
    ret.should == "aaa<![CDATA[bbb]]>ccc"
    ret = p.process("aaa<![CDATA[bbb]]>cccAAA<![CDATA[BBB]]>CCC")
    ret.should == "aaa<![CDATA[bbb]]>cccAAA<![CDATA[BBB]]>CCC"
  end

  specify "replace <% %>" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<%aaa%>")
    ret.should == "<![CDATA[<%aaa%>]]>"
    ret = p.process("a<%b%>c<%d%>e")
    ret.should == "a<![CDATA[<%b%>]]>c<![CDATA[<%d%>]]>e"
  end

  specify "replace <%= %>" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<%=aaa%>")
    ret.should == "<![CDATA[<%=aaa%>]]>"
    ret = p.process("a<%b%>c<%=d%>e")
    ret.should == "a<![CDATA[<%b%>]]>c<![CDATA[<%=d%>]]>e"
  end

  specify "don't replace <%= %> in cdata" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<![CDATA[<%=aaa%>]]>")
    ret.should == "<![CDATA[<%=aaa%>]]>"
    ret = p.process("a<%=b%>c<![CDATA[<%d%>]]>e")
    ret.should == "a<![CDATA[<%=b%>]]>c<![CDATA[<%d%>]]>e"

    s = <<-END
      <![CDATA[
        <% a = 1 %>
        1 + 2 = <%= a + 2 %>
      ]]>
    END
    p.process(s).should == s
  end

  specify "not replace <%= %> in cdata in real" do
    text = <<-'END'
    <ul>
      <li am:foreach="data" am:if='$_.name =~ /amrita/'>
        <![CDATA[
          <%
            name = $_.name
            description = ($_.description or $_.name)
          %>
          <a href="http://raa.ruby-lang.org/list.rhtml?name=<%= name %>"><%= description %></a>
        ]]>
      </li>
    </ul>
    END
    ret = Amrita2::Core::PreProcessor.new.process(text)

    text.should == ret
  end

  specify "section" do
    pp = Amrita2::Core::PreProcessor.new
    ret = pp.process("aaa<%(Section1)xxx%>bbb")
    ret.should == "aaabbb"
    pp.sections[:Section1].should == "xxx"
  end

  specify "section and cdata" do
    pp = Amrita2::Core::PreProcessor.new
    ret = pp.process("aaa<%(Section1)xxx%>bbb<%ccc%>ddd")
    ret.should == "aaabbb<![CDATA[<%ccc%>]]>ddd"
    pp.sections[:Section1].should == "xxx"
  end

  specify "two sections and cdata" do
    pp = Amrita2::Core::PreProcessor.new
    ret = pp.process("aaa<%(Section1)xxx%>bbb<%ccc%>ddd<%(Section2)yyy%>eee")
    ret.should == "aaabbb<![CDATA[<%ccc%>]]>dddeee"
    pp.sections[:Section1].should == "xxx"
    pp.sections[:Section2].should == "yyy"
  end

  specify "two sections and cdata with nest" do
    pp = Amrita2::Core::PreProcessor.new
    ret = pp.process("aaa<%(Section1)<%=1%>xxx%>bbb<%%<%=2%>ccc%>ddd<%(Section2)<%=3%>yyy%>eee")
    ret.should == "aaabbb<![CDATA[<%%<%=2%>ccc%>]]>dddeee"
    pp.sections[:Section1].should == "<%=1%>xxx"
    pp.sections[:Section2].should == "<%=3%>yyy"
  end

  specify "nbsp" do
    pp = Amrita2::Core::PreProcessor.new
    ret = pp.process("&nbsp; &amp;")
    ret.should == "&nbsp; &amp;"
  end
end


context "amxml inline" do
  include Amrita2

  specify "inline" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<<a>>") #"
    ret.should == "<a />"
    ret = p.process("<<a:xxx>>")
    ret.should == "<a am:src='xxx' />"
    ret = p.process("<<p class='text':aaa>>")
    ret.should == "<p am:src='aaa' class='text' />"
    ret = p.process("<<a href='http://amrita2.rubyforge.org/':xxx>>")
    ret.should == "<a am:src='xxx' href='http://amrita2.rubyforge.org/' />"
    ret = p.process(%[<<a href="http://amrita2.rubyforge.org/":xxx>>])
    ret.should == %[<a am:src='xxx' href="http://amrita2.rubyforge.org/" />]

    p.process("<<h3:title>>").should == "<h3 am:src='title' />"
    p.process("<<h3 class='title':title>>").should == "<h3 am:src='title' class='title' />"
  end

  specify "inline skipif" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<<a ?[a == 1]>>")
    ret.should == "<a am:skipif='not(a == 1)' />"
    ret = p.process("<<a ?![a == 1]>>")
    ret.should == "<a am:skipif='a == 1' />"
  end

  specify "inline anonymous tag" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("<<:xxx>>")
    ret.should == "<_ am:src='xxx' />"
  end

  specify "inline filter" do
    r = %r[#{Amrita2::Core::PreProcessor::AmXml::S_SRC_AND_FILTERS }]
    ":a|A|B".should match(r)
    ":a|A[aaa]|B[abb]".should match(r)

    p = Amrita2::Core::PreProcessor.new


    ret = p.process(%[<<a:xxx|Attr[:href=>:url, :body=>:description]>>])
    ret.should == %[<a am:src='xxx|Attr[:href=>:url, :body=>:description]' />]
    ret = p.process(%[<<a:xxx|Attr[:href=>:url, :body=>:description]>>])
    ret.should == %[<a am:src='xxx|Attr[:href=>:url, :body=>:description]' />]

    ret = p.process(%[<<a:|Attr[:href=>:url, :body=>:description]>>])
    ret.should == %[<a am:filter='Attr[:href=>:url, :body=>:description]' />]

    ret = p.process(%[<<:|Each[:class=>[:odd, :even]]|Attr[:class,:body=>:no]>>])
    ret.should == %[<_ am:filter='Each[:class=>[:odd, :even]]|Attr[:class,:body=>:no]' />]
    ret = p.process(%[<<:|Each[:class=>[:odd, :even]]|\nAttr[:class,:body=>:no]>>])
    ret.should == %[<_ am:filter='Each[:class=>[:odd, :even]]|\nAttr[:class,:body=>:no]' />]
    ret = p.process(%[<<a:website\|Attr>>])
    ret.should == %[<a am:src='website|Attr' />]
  end

  specify "css selecter like" do
    p = Amrita2::Core::PreProcessor.new

    ret = p.process("<<aaa#bbb>><<ccc.ddd>>")
    ret.should == '<aaa id="bbb" /><ccc class="ddd" />'
    ret = p.process("<<a#bbb href=\"http://www.brain-tokyo.jp/index.html#a\">>")
    ret.should == '<a href="http://www.brain-tokyo.jp/index.html#a" id="bbb" />'

    ret = p.process("<<a.bbb#ccc href=\"http://www.brain-tokyo.jp/index.html#a\">>")
    ret.should == '<a href="http://www.brain-tokyo.jp/index.html#a" class="bbb" id="ccc" />'
    ret = p.process("<<a#bbb.ccc href=\"http://www.brain-tokyo.jp/index.html#a\">>")
    ret.should == '<a href="http://www.brain-tokyo.jp/index.html#a" id="bbb" class="ccc" />'

    ret = p.process("<<div.cart-title>>")
    ret.should == '<div class="cart-title" />'
  end
  specify "css selecter like" do
    p = Amrita2::Core::PreProcessor.new

    ret = p.process("<<p %= 1+2 >>")
    ret.should == '<p><![CDATA[<%= 1+2 %>]]></p>'
  end
end

context "amxml block" do
  include Amrita2

  specify "simple" do

    p = Amrita2::Core::PreProcessor.new
    p.process(<<-END).should_be_samexml_as("<_ am:src='title' /> ")
      <<:title>>
    END

    p = Amrita2::Core::PreProcessor.new
    p.process(<<-END).should_be_samexml_as("<html>test</html>")
      <<html<
        test
    END

    p = Amrita2::Core::PreProcessor.new
    p.process(<<-END).should_be_samexml_as('<div class="cart-title">test</div>')
      <<div.cart-title<
        test
    END

    p.process(<<-END).should_be_samexml_as(<<-END)
      <<html<
        <<body<
          <p>12345</p>
    END
     <html><body><p>12345</p> </body></html>
    END

    p.process(<<-END).should_be_samexml_as(<<-END)
      <<html<
        <<head<
          <title>title1</title>
        <<body<
          <p>12345</p>
    END
      <html>
        <head><title>title1</title></head>
        <body><p>12345</p></body>
      </html>
    END


    p.process(<<-END).should_be_samexml_as("<div class='entry' am:src='products'><h3 am:src='title' /> </div>")
    <<div class="entry":products<
      <<h3:title>>
    END

  end

  specify "src" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
    <<table<
      <<tr:clients|NVar[:surname, :firstname, :email]<
        <td>$1, $2</td>
        <td><a href="mailto:$3">$3</a></td>
    END
    <table>
      <tr am:src='clients|NVar[:surname, :firstname, :email]'>
        <td>$1, $2</td>
        <td><a href='mailto:$3'>$3</a></td>
      </tr>
    </table>
    END
  end

  specify "for" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
    << [1..5] <
      abc
    END
    <_ am:for='1..5'>abc</_>
    END
  end

  specify "for and cond" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
    << [1..5] <
      << ?[ ($_%2) == 0] <
        abc
    END
    <_ am:for='1..5'>\n<_ am:skipif='not(($_%2) == 0)'>abc</_>\n</_>
    END
  end

  specify "for and cond and value" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
      << [1..5] <
        << ?[ ($_%2) == 0] <
          << { :no => $_, :square=> $_*$_ }<
            <<li<
              <<:no>> * <<:no>> = <<:square>>
    END
    <_ am:for='1..5'>
      <_ am:skipif='not(($_%2) == 0)'>
        <_ am:v='HashDelegator.new($_) { {:no => $_, :square=> $_*$_} }'>
         <li>
           <_ am:src='no' />*<_ am:src='no' />=<_ am:src='square' />
         </li>
        </_>
      </_>
    </_>
    END
  end

  specify "filter and value" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
        <<div#cart
          {
            :style => @cart.items.empty? ? "display: none" : nil
          } <
          %= render(:partial => "cart", :object => @cart)

    END
<div am:v='HashDelegator.new($_) { {:style => @cart.items.empty? ? "display: none" : nil} }' id="cart">
<![CDATA[<%
 _erbout.concat(eval(%{ render(:partial => "cart", :object => @cart)}).to_s)
%>]]>
</div>
END
  end

  specify "multi line" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).should_be_samexml_as(<<-END)
    <<table<
      <<tr:clients
          |NVar[:surname, :firstname, :email]<
        <td>$1, $2</td>
        <td><a href="mailto:$3">$3</a></td>
    END
    <table>
      <tr am:src='clients|NVar[:surname, :firstname, :email]'>
        <td>$1, $2</td>
        <td><a href='mailto:$3'>$3</a></td>
      </tr>
    </table>
    END

    p.process(<<-END).should_be_samexml_as(<<-END)
    <<table<
      <<tr:lang_list
          |ToHash[:name=>:name, :author=>:author]
                    |Each[:class=>["odd", "even"]]
                    |Attr[:class] <
         <td />
    END
    <table>
      <tr am:src='lang_list|ToHash[:name=>:name, :author=>:author]
                    |Each[:class=>[\"odd\", \"even\"]]
                    |Attr[:class]'>
        <td />
      </tr>
    </table>
    END
  end

  specify "inline ruby" do
    p = Amrita2::Core::PreProcessor.new
    expected = <<-END
    <_ am:for='1..5'>
      <_ am:skipif='not(($_%2) == 0)'>
        <_ am:v='HashDelegator.new($_) { {:no => $_,\n               :square=> $_*$_} }'>
         <li>
           <_ am:src='no' />*<_ am:src='no' />=<_ am:src='square' />
         </li>
        </_>
      </_>
    </_>
    END

    a = p.process(<<-END)
      <<
        [1..5] <
        <<?[ ($_%2) == 0

           ] <
          << {
               :no => $_,
               :square=> $_*$_
             } <

            <<li<
              <<:no>> * <<:no>> = <<:square>>
    END
    a.should_be_samexml_as(expected)
    a = p.process(<<-END)
      << [1..5] <
        << ?[ ($_%2) == 0] <
          << { :no => $_, :square=> $_*$_ }<
            <<li<
              <<:no>> * <<:no>> = <<:square>>
    END
    a.gsub(/\s+/m, ' ').should_be_samexml_as(expected.gsub(/\s+/m, ' '))
  end
end

context "amxml erb" do
  include Amrita2

  specify "simple" do
    p = Amrita2::Core::PreProcessor.new

    p.process(<<-END).gsub("% ", '%').should == (<<-END).strip
      <<div<
        % a = 1
    END
<div ><![CDATA[<%
a = 1
%>]]>
</div>
    END
    p.process(<<-END).gsub("% ", '%').gsub(" \n", "\n").should == (<<-END).strip
      <<div<
        %= a + 1
    END
<div ><![CDATA[<%
 _erbout.concat(eval(%{ a + 1}).to_s)
%>]]>
</div>
    END

    p.process(<<-END).should_be_samexml_as(<<-END)
      <<div<
        %  form_remote_tag :url => { :action => :add_to_cart, :id => $_ } do
        %=   submit_tag "Add to Cart"
        %  end
    END
<div>
<% form_remote_tag :url => {  :action => :add_to_cart, :id => $_ } do
_erbout.concat(eval(%{ submit_tag "Add to Cart"}).to_s)
end %>
</div>
    END
  end
end

context "cell processor" do
  include Amrita2

  specify "not cell" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("||aaa").should == false
    l.cells.size.should == 0
  end

  specify "1 cell" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa|")
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>aaa</td>"

    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa") # omit the last |
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>aaa</td>"
  end

  specify "1 cell with spaces" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|   ||aaa|").should == true
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>aaa</td>"
  end

  specify "2cell" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa|bbb")
    l.cells.size.should == 2
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    c = l.cells[1]
    c[:text].should == "bbb"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>aaa</td><td>bbb</td>"
  end
  specify "th and 2cell" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa||bbb|ccc|")
    l.cells.size.should == 3
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :th
    c = l.cells[1]
    c[:text].should == "bbb"
    c[:tag].should == :td
    l.get_result_xml.should == "<th>aaa</th><td>bbb</td><td>ccc</td>"
  end

  specify "1cell and id" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa|")
    l.parse_line("|id||xxx|")
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    c["id"].should == "xxx"
    l.get_result_xml.should == "<td id='xxx'>aaa</td>"
  end

  specify "1 cell 2 lines" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line("|||aaa|")
    l.parse_line("|||bbb|")
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa\nbbb"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>aaa\nbbb</td>"
  end

  specify "proc" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     "(#{s})"
    end
    l.parse_line("|||aaa|")
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "aaa"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>(aaa)</td>"
  end

  specify "real td cells" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line(%[        |||$1, $2|<a href="mailto:$3">$3</a>|])
    l.cells.size.should == 2
    c = l.cells.first
    c[:text].should == "$1, $2"
    c[:tag].should == :td
    l.get_result_xml.should == "<td>$1, $2</td><td><a href=\"mailto:$3\">$3</a></td>"
  end

  specify "filter in cell" do
    l = Amrita2::Core::PreProcessor::LineProcessor.new do |s|
     s
    end
    l.parse_line(%q[        |||<<a user\\|Attr[:href=>:url]>>|])
    l.cells.size.should == 1
    c = l.cells.first
    c[:text].should == "<<a user|Attr[:href=>:url]>>"
    c[:tag].should == :td
    l.get_result_xml.should == "<td><<a user|Attr[:href=>:url]>></td>"
  end
end

context "cell line" do
  specify "1 cell" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process("|||aaa|")
    ret.should == "<td>aaa</td>"
  end

  specify "table" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process <<-END
    <<table<
      <<tr:clients|NVar[:surname, :firstname, :email]<
        |||$1, $2|<a href="mailto:$3">$3</a>|
    END
    ret.should_be_samexml_as <<-END
    <table>
      <tr am:src='clients|NVar[:surname, :firstname, :email]' >
        <td>$1, $2</td>
        <td><a href="mailto:$3">$3</a></td>
      </tr>
    </table>
    END
  end

  specify "table with class" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process <<-END
    <<table<
      <<tr:clients|NVar[:surname, :firstname, :email]<
        |class||cell1 |cell2                     |
        |     ||$1, $2|<a href="mailto:$3">$3</a>|
    END
    ret.should_be_samexml_as <<-END
    <table>
      <tr am:src='clients|NVar[:surname, :firstname, :email]' >
        <td class='cell1'>$1, $2</td>
        <td class='cell2'><a href="mailto:$3">$3</a></td>
      </tr>
    </table>
    END
  end

  specify "table with tr line" do
    p = Amrita2::Core::PreProcessor.new
    ret = p.process <<-END
    <<table<
      <<<------------------------------------------
        |class||cell1 |cell2                     |
        |     ||$1, $2|<a href="mailto:$3">$3</a>|
    END
    ret.should_be_samexml_as <<-END
    <table>
      <tr>
        <td class='cell1'>$1, $2</td>
        <td class='cell2'><a href="mailto:$3">$3</a></td>
      </tr>
    </table>
    END
  end
end
