
require 'amrita2'
require 'amrita2/testsupport'

context "normalize xml"do
  include Amrita2::TestSupport
  
  specify "single tag" do
    normalize("<img />").should == '<img />'
    normalize("<a />").should == '<a />'
  end
  
  specify "single tag with attributes" do
    normalize("<a aaa='aaa' bbb='bbb'/>").should == '<a aaa="aaa" bbb="bbb" />'
    normalize("<a bbb='bbb' aaa='aaa'/>").should == '<a aaa="aaa" bbb="bbb" />'
  end

  specify "tag with text" do
    normalize("<a>xx</a>").should == "<a>\nxx\n</a>"
  end
  
  specify "nested tag" do
    normalize("<a><img></a>").should == "<a>\n<img />\n</a>"
  end
  
  specify "nested tag with text" do
    normalize("<a><_>xx</_></a>").should == "<a>\n<_>\nxx\n</_>\n</a>"
  end

  specify "text" do
    normalize("abc").should == "abc"
    normalize(" abc ").should == "abc"
    normalize(" \n abc  \n ").should == "abc"
    normalize(" \n abc  \n abc").should == "abc abc"
    normalize(" \n a b c  \n a b c\n").should == "a b c a b c"
    normalize("<a> \n a b c  \n a b c\n</a>\n").should == "<a>\na b c a b c\n</a>"
  end

  specify "single tag with space" do 
    normalize('<_ am:src="title" />').should == '<_ am:src="title" />'
    normalize(" <_ am:src='title' /> \n").should == '<_ am:src="title">'
  end
end


context "should_same_xml_as" do
  
  specify "xml" do
    a = "<div class='aaa'><a href='bbb' title='ccc'>ddd</a></div>"
    b = "<div class='aaa'><a href='bbb' title='ccc'>ddd</a></div>"
    a.should_be_samexml_as b
    Amrita2::SanitizedString[a].should_be_samexml_as b
  end

  specify "change order of attributes" do
    a = "<div class='aaa'><a href='bbb' title='ccc'>ddd</a></div>"
    a.should_be_samexml_as "<div class='aaa'><a title='ccc' href='bbb' >ddd</a></div>"
  end

  specify "add extra spaces" do
    a = "<div class='aaa'> <a href='bbb' title='ccc'>\nddd</a>\n</div>" 
    b = "<div class='aaa'>\n<a href='bbb' title='ccc'>\nddd</a></div>"
    a.should_be_samexml_as b
  end
  
  specify "ignore space between tags" do
    a = "<ul><li>1</li><li>2</li></ul>" 
    b = "<ul><li>1</li> <li>2</li></ul>" 
    a.should_be_samexml_as b
  end
  
  specify "ignore space before tags" do
    a = "\n aaa<span>bbb</san>ccc \n"
    b = "aaa<span>bbb</san>ccc"
    a.should_be_samexml_as b

    a = Amrita2::SanitizedString["\nhook can write to stream direct<div>hook can render self</div>and add anything after that\n"]
    b = "hook can write to stream direct<div>hook can render self</div>and add anything after that"
    a.should_be_samexml_as b
  end

  specify "ignore newlines" do 
    a = "<th rowspan=\"2\">Red<br></br>eyes  </th>"
    b = "<th rowspan=\"2\">Red<br />eyes</th>"
    a.should_be_samexml_as b
  end

end

