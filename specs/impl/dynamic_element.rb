
require 'rexml/element'
require 'amrita2'

include Amrita2

context "check hpricot" do
  specify "no child" do
    text = %q[<span am:src='aaa' />]
    e = Hpricot.make(text)[0]
    e.enum_for(:each_child).to_a.size.should == 0
  end
  
  specify "one child" do
    text = %q[<span am:src='aaa'><span am:src='bbb' /></span>]
    e = Hpricot.make(text)[0]
    e.enum_for(:each_child).to_a.size.should == 1
  end
  
  specify "two children" do
    text = %q[<span am:src='aaa'><span am:src='bbb' /><span am:src='ccc' /></span>]
    e = Hpricot.make(text, :xml=>true)[0]
    e.children.size == 2
    e.enum_for(:each_child).to_a.size.should == 2
  end
end

context "has_dynamic?" do
  specify "single" do
    text = %q[<span am:src='aaa' />]
    t = Amrita2::Template.new(text)
    t.setup
    t.root.has_dynamic?.should == true
    t.root.children.first.has_dynamic?.should == false
  end
  
  specify "nest" do
    text = %q[<span am:src='aaa'><span am:src='bbb' /></span>]
    t = Template.new(text)
    t.setup
    t.root.has_dynamic?.should == true
    t.root.children.first.has_dynamic?.should == true
    t.root.children.first.children.first.has_dynamic?.should == false
  end
  
  specify "nest2" do
    text = %q[<span am:src='aaa'><span am:src='bbb' /><span am:src='ccc' /></span>]
    t = Template.new(text)
    t.setup
    t.root.has_dynamic?.should == true
    aaa = t.root.children.first
    aaa.element.children.size.should == 2
    aaa.children.size.should == 2
    bbb, ccc = *aaa.children
    bbb.should be_dynamic
    ccc.should be_dynamic
  end
  
  specify "has text" do
    text = %q[<span am:src='aaa'>text</span>]
    t = Template.new(text)
    t.setup
    t.root.has_dynamic?.should == true
    t.root.children.first.has_dynamic?.should == false
  end
  
  specify "has erb" do
    text = %q[<span am:src='aaa'><% 1 %></span>]
    t = Template.new(text)
    t.setup
    t.root.has_dynamic?.should == true
    t.root.children.first.has_dynamic?.should == true
  end
end

context "has_ruby?" do
  specify "single" do
    text = %q[<span am:src='aaa' />]
    t = Amrita2::Template.new(text)
    t.setup
    t.root.has_ruby?.should == nil
    t.root.children.first.has_ruby?.should == nil
  end
  
  specify "has erb" do
    text = %q[<span am:src='aaa'><% 1 %></span>]
    t = Template.new(text)
    t.setup
    t.root.has_ruby?.should == true
    t.root.children.first.has_ruby?.should == true
  end
end
