require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'
require 'amrita2/template'
require 'amrita2/gettext'
require 'gettext/utils'
require 'fileutils'

include Amrita2
include Amrita2::Filters
include Amrita2::GetTextBridge

require File::dirname(__FILE__) + '/gettext_util'

$KCODE='u'

context "gettext static element" do
  include Amrita2GetTextTestSupport
  TEMPFILE='/tmp/test.a2html'

  specify "every text becomes an entry of po" do
    Locale.set_current("ja", "JP", "utf8")
    test_gettext("test1",
                 TEMPFILE,
                 "<span>test message</span>",
                 [
                   %r[#: /tmp/test.a2html:-],
                   %r[msgid "test message"],
                 ],
                 {},
                 "テストメッセージ"
                 ) do |l|
      l.sub!(%r[msgid "test message"\nmsgstr ""]m,
              %[msgid "test message"\nmsgstr "テストメッセージ"])
      l
    end
  end

  specify "strip messages" do
    Locale.set_current("ja", "JP", "utf8")
    test_gettext("test1",
                 TEMPFILE,
                 "<span> test message </span>",
                 [
                   %r[#: /tmp/test.a2html:-],
                   %r[msgid "test message"],
                 ],
                 {},
                 "テストメッセージ"
                 ) do |l|
      l.sub!(%r[msgid "test message"\nmsgstr ""]m,
              %[msgid "test message"\nmsgstr "テストメッセージ"])
      l
    end
  end

  specify "two messages" do
    test_gettext("test3",
                 TEMPFILE,
                 "<div><span> test message1 </span><span>test message2</span></div>",
                 [
                   %r[#: /tmp/test.a2html:-],
                   %r[msgid "test message1"],
                   %r[msgid "test message2"],
                 ],
                 {},
                 "<div>あああいいい</div>"
                 ) do |l|
      l.sub!(%r[msgid "test message1"\nmsgstr ""]m,
              %[msgid "test message1"\nmsgstr "あああ"])
      l.sub!(%r[msgid "test message2"\nmsgstr ""]m,
              %[msgid "test message2"\nmsgstr "いいい"])
      l
    end
  end

  specify "text with html comment" do
    Locale.set_current("ja", "JP", "utf8")
    test_gettext("test1",
                 TEMPFILE,
                 "<!-- comment --><span>test message</span>",
                 [
                   %r[#: /tmp/test.a2html:-],
                   %r[msgid "test message"],
                 ],
                 {},
                 "<!-- comment -->\nテストメッセージ"
                 ) do |l|
      l.sub!(%r[msgid "test message"\nmsgstr ""]m,
              %[msgid "test message"\nmsgstr "テストメッセージ"])
      l
    end
  end
end

context "format with gettext" do
  include Amrita2GetTextTestSupport

  specify "format1" do
    Locale.set_current("ja", "JP", "utf8")
    test_gettext(
                 "format1",
                 TEMPFILE,
                 "<span>%{filename} is %{filesize} byte</span>",
                 [
                   %r[#: #{TEMPFILE}:-],
                   %r[msgid "%\{filename\} is %\{filesize\} byte"],
                 ],
                 {
                   :filename => "foo.rb",
                   :filesize => 100
                 },
                 "foo.rbの大きさは100バイトです"
                 ) do |l|
      l.sub!(%r[msgid "%\{filename\} is %\{filesize\} byte"\nmsgstr ""]m,
              %[msgid "%\{filename\} is %\{filesize\} byte"\nmsgstr "%{filename}の大きさは%{filesize}バイトです"])
      l
    end
  end
end
