
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2
include Amrita2::Filters
include Amrita2::Runtime


context "text inside CDATA is evaluated as ERb" do
  include Amrita2::Runtime
   setup do
     @t = Amrita2::Template.new('<span><![CDATA[ <%= 1 + 1 %> ]]></span>')
   end

   specify "TOPLEVEL Binding" do
     @t.test_with({}) do |result|
       result.strip.should == "2"
     end
   end
end

context "you can omit CDATA in most case because AMXML preprocessor inserts it." do
  include Amrita2::Runtime
   setup do
     @t = Amrita2::Template.new('<span><%= 1 + 1 %></span>')
   end

   specify "TOPLEVEL Binding" do
     @t.test_with({}) do |result|
       result.strip.should == "2"
     end
   end
end

context "Minimum mixed Template" do
  include Amrita2::Runtime
   setup do
     @t = Amrita2::Template.new('<span><span am:src="aaa" /><![CDATA[ <%= 1 + 1 %> ]]></span>')
   end

   specify "TOPLEVEL Binding" do
     @t.test_with(:aaa=>' 1 + 1 =') do |result|
       result.strip.should == "1 + 1 = 2"
     end
   end
end

context "Minimum ERB Template with variable" do
  include Amrita2::Runtime
  setup do
    @t = Amrita2::Template.new('<span><![CDATA[ <%= a + 1 %> ]]></span>')
  end

  specify "TOPLEVEL Binding" do
    proc { @t.render_with({}) }.should raise_error(NameError)
  end

  specify "set Binding" do
    a = 100
    @t.test_with(binding) do |result|
      result.strip.should == "101"
    end
  end
end

context "<%% ... %%>" do
  include Amrita2::Runtime
   specify " " do
     t = Amrita2::Template.new('<%%= 1 + 1 %>')
     t.test_with({}) do |result|
       result.strip.should == "<%= 1 + 1 %>"
      t = Amrita2::Template.new(result.dup)
      t.test_with({}) do |result|
        result.strip.should == "2"
      end
    end
  end
end


context '#{..} in erb' do
  include Amrita2::Runtime
  specify " " do
    t = Amrita2::Template.new('<%= %[#{1+1}]  %>')
    t.test_with({}) do |result|
      result.strip.should == "2"
    end
  end
end

context "<%% ... %%>" do
  include Amrita2::Runtime
  specify " " do
    text = <<-END
    <span>
      <%= $_[:term1] %> + <%= $_[:term2] %> = <%= $_[:term1].to_i  +  $_[:term2].to_i %>
      <%= $_[:term1] %> - <%= $_[:term2] %> = <%= $_[:term1].to_i  -  $_[:term2].to_i %>
    </span>
    END

    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(:term1=>20, :term2=>30) do |result|
      result.should_be_samexml_as "20  +  30  =  50  20  -  30  =  -10"
    end
  end
end

context '<%= %> in erb' do
  include Amrita2::Runtime
  specify " " do
    t = Amrita2::Template.new('<%= %[<%= 1+1 %%>]  %>')
    t.set_trace(STDOUT)
    t.test_with({}) do |result|
      result.strip.should == "<%= 1+1 %>"
      t = Amrita2::Template.new(result.dup)
      t.test_with({}) do |result|
        result.strip.should == "2"
      end
    end
  end
end

context "amxml block erb" do
  include Amrita2::Runtime
  specify " " do
    text = <<-END
    <<div<
      % abc = 100 + 20 + 3
      <<:abc>>
    END
    abc = nil
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should_be_samexml_as "<div>123</div>"
    end
  end

  specify "block " do
    text = <<-END
    <<div<
      % sum = 0
      % 10.times do |i|
      %  sum += i
      % end
      <<span class='sum':sum>>
    END

    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should_be_samexml_as "<div><span class='sum'>45</span></div>"
    end
  end
end

context "amxml cdata_erb block('<<%<')" do
  include Amrita2::Runtime
  specify " " do
    text = <<-END
    <<ul<
      <<%<
        <% 3.times do |n| %>
          <li><%= n + 1 %></li>
        <% end %>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should_be_samexml_as "<ul><li>1</li><li>2</li><li>3</li></ul>"
    end
  end
end

context "amxml cdata_erb block(keep cdata)" do
  include Amrita2::Runtime
  specify " " do
    text = <<-END
    <<div<
      <<%<
        <![CDATA[aaa]]>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should == "<div>        <![CDATA[aaa\n]]></div>"
    end
  end
end

context "erb in amxml " do
  include Amrita2::Runtime
  specify " " do
    text = "<<p %= 1+2 >>"
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should == "<p>3</p>"
    end
  end
end
