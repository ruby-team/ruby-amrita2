
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2
include Amrita2::Filters
include Amrita2::Runtime


context "Inline Ruby" do
  include Amrita2::Runtime
  specify "set context value" do
    text = <<-END
    <div>
    <%
      $_ = { :aaa => "abc" + a }
    %>
    <span am:src="aaa">abc</span>
    </div>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    a = "xyz"
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>abcxyz</div>")
    end
  end

  specify "with cdata" do
    text = <<-'END'
    <div>
    <%
      $_ = { :aaa => SanitizedString["<em>#{a}</em>"] }
    %>
    <span am:src="aaa">abc</span>
    </div>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    a = "xyz"
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div><em>xyz</em></div>")
    end
  end

  specify "with an array of struct" do
    text = <<-'END'
    <ul>
      <li am:for="data" am:skipif='$_.name !~ /amrita/'>
        <%
          $_ = {
             :url => "http://raa.ruby-lang.org/list.rhtml?name=#{$_.name}",
             :description => ($_.description or $_.name)
          }
        %>
        <a am:filter='Attr[:href=>:url, :body=>:description]'/>
      </li>
    </ul>
    END
    s = Struct.new(:name, :description)
    data = [
      s.new('amrita', 'Amrita template library'),
      s.new('amrita2'),
      s.new('Ruby on Rails', 'this will be passed'),
    ]
    t = Amrita2::Template.new(text)
    t.inline_ruby = true
    t.test_with(binding) do |result|
      result.should_be_samexml_as <<-END
      <ul>
        <li>
          <a href='http://raa.ruby-lang.org/list.rhtml?name=amrita'>Amrita template library</a>
        </li>
        <li>
          <a href='http://raa.ruby-lang.org/list.rhtml?name=amrita2'>amrita2</a>
        </li>
      </ul>
      END
    end
  end

  specify "with erb" do
    text = <<-'END'
    <ul>
      <li am:for="data" am:skipif='$_.name !~ /amrita/'>
        <![CDATA[
          <%
            name = $_.name
            description = ($_.description or $_.name)
          %>
          <a href="http://raa.ruby-lang.org/list.rhtml?name=<%= name %>"><%= description %></a>
        ]]>
      </li>
    </ul>
    END
    s = Struct.new(:name, :description)
    data = [
      s.new('amrita', 'Amrita template library'),
      s.new('amrita2'),
      s.new('Ruby on Rails', 'this will be passed'),
    ]
    t = Amrita2::Template.new(text)
    t.inline_ruby = true
    t.test_with(binding) do |result|
      result.should_be_samexml_as <<-END
      <ul>
        <li>
          <a href='http://raa.ruby-lang.org/list.rhtml?name=amrita'>Amrita template library</a>
        </li>
        <li>
          <a href='http://raa.ruby-lang.org/list.rhtml?name=amrita2'>amrita2</a>
        </li>
      </ul>
      END
    end
  end
end

context "Logic IF" do
  include Amrita2::Runtime
  setup do
    @t = Amrita2::Template.new('<span am:skipif="a == 1">abc</span>', :inline_ruby)
  end

  specify "if true" do
    a = 1
    @t.test_with(binding) do |result|
      result.should_be_samexml_as("")
    end
  end

  specify "false" do
    a = 0
    @t.test_with(binding) do |result|
      result.should_be_samexml_as("abc")
    end
  end

  specify "with loop" do
    text = <<-END
    <ul>
      <li am:for="1..5" am:skipif="($_ % 2) == 0" />
    </ul>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<ul><li>1</li><li>3</li><li>5</li></ul>")
    end
  end
end

context "Logic IF amxml" do
  include Amrita2::Runtime

  specify "if" do
    t = Amrita2::Template.new('<<img src="aaa" ?[a == 1]>>')
    a = 1
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<img src='aaa' />")
    end
    a = 0
    t.test_with(binding) do |result|
      result.should_be_samexml_as("")
    end
  end

  specify "unless" do
    t = Amrita2::Template.new('<<img src="aaa" ?![a == 1]>>')
    a = 0
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<img src='aaa' />")
    end
    a = 1
    t.test_with(binding) do |result|
      result.should_be_samexml_as("")
    end
  end

  specify "if" do
    t = Amrita2::Template.new('<<a href="aaa":xxx ?[a == 1]>>')
    xxx = "123"
    a = 1
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<a href='aaa'>123</a>")
    end
    a = 0
    t.test_with(binding) do |result|
      result.should_be_samexml_as("")
    end
  end

  specify "with loop" do
    text = <<-END
    <ul>
      <<li :list ?![($_ % 2) == 0] >>
    </ul>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(:list=>1..5) do |result|
      result.should_be_samexml_as("<ul><li>1</li><li>3</li><li>5</li></ul>")
    end

    text = <<-END
    <ul>
      <<li :list ?[($_ % 2) == 0] >>
    </ul>
    END
    t = Amrita2::Template.new(text, :inline_ruby)
    t.test_with(:list=>1..5) do |result|
      result.should_be_samexml_as("<ul><li>2</li><li>4</li></ul>")
    end
  end
end

context "Logic for" do
  include Amrita2::Runtime
  setup do
    @t = Amrita2::Template.new('<div><span am:for="a" /></div>', :inline_ruby)
  end

  specify "0" do
    a = []
    @t.test_with(binding) do |result|
      result.should_be_samexml_as("<div></div>")
    end
  end
  specify "1" do
    a = [1]
    @t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>1</div>")
    end
  end

  specify "3" do
    a = [1, 2, 3]
    @t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>123</div>")
    end
  end
end

context "Logic value" do
  include Amrita2::Runtime
  specify "1" do
    t = Amrita2::Template.new('<div><span am:v="a * 2" /></div>', :inline_ruby)
    a = 1
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>2</div>")
    end
  end

  specify "method call" do
    t = Amrita2::Template.new('<div><span am:v="aaa(a)" /></div>', :inline_ruby)
    a = 123
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>246</div>")
    end
  end

  def aaa(x)
    x*2
  end

  specify "filter" do
    t = Amrita2::Template.new('<div><span am:src="a" am:v="$_ * 2" /></div>')
    t.inline_ruby = true
    a = 1
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>2</div>")
    end
    a = "abc"
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>abcabc</div>")
    end
  end

  specify "quotation" do
    t = Amrita2::Template.new(%[<div><span am:v=" 'abc' * a" /></div>], :inline_ruby)
    a = 2
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>abcabc</div>")
    end

    t = Amrita2::Template.new(%[<div><span am:v=' "abc" * a' /></div>], :inline_ruby)
    a = 2
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>abcabc</div>")
    end
  end

  specify '#{ ...} in am:v' do
    t = Amrita2::Template.new(%q[<div><span am:v=" %[#{a * 2}] " /></div>], :inline_ruby)
    a = 2
    t.test_with(binding) do |result|
      result.should_be_samexml_as("<div>4</div>")
    end
  end

  specify 'loop with if' do
    t = Amrita2::Template.new <<-END
    <<ul<
      << [1..5] <
        << ?[ ($_%2) == 0] <
          <<li<
            <%= $_ %>
    END
    t.test_with(binding) do |result|
      result.should_be_samexml_as <<-END
      <ul><li>2</li><li>4</li></ul>
      END
    end
  end

  specify 'all' do
    t = Amrita2::Template.new <<-END
    <<ul<
      << [1..5] <
        << ?[ ($_%2) == 0] <
          << { :no => $_, :no2 => $_ ,:square=> $_*$_ }<
            <<li<
              <<:no>> * <<:no2>> = <<:square>>
    END
    t.test_with(binding) do |result|
      result.should_be_samexml_as <<-END
      <ul>
        <li>2 * 2 = 4</li>
        <li>4 * 4 = 16</li>
      </ul>
      END
    end
  end

  specify 'all various format' do
    t = Amrita2::Template.new <<-END
    <<ul<
      <<
        [1..6] <
        <<?[ ($_%2) == 0

           ] <
          << {
               :no => $_,
               :no2 => $_,
               :square=> $_*$_
             } <

            <<li<
              <<:no>> * <<:no2>> = <<:square>>
    END
    t.test_with(binding) do |result|
      result.should_be_samexml_as <<-END
      <ul>
        <li>2 * 2 = 4</li>
        <li>4 * 4 = 16</li>
        <li>6 * 6 = 36</li>
      </ul>
      END
    end
  end
end

context "quotes in erb" do
  include Amrita2::Runtime
  specify "double quote" do
    text = %q[<%= "#{1+2}" %>]
    t = Amrita2::Template.new(text)
    t.test_with(binding) do |result|
      result.should_be_samexml_as("3")
    end
  end

  specify "making quote in erb" do
    text = %q[<%%= "#{1+<%= 1+2 %>}" %>]
    t = Amrita2::Template.new(text)
    t.test_with(binding) do |result|
      result.should_be_samexml_as(%q[ <%= "#{1+3}" %> ])
      t = Amrita2::Template.new(result)
      t.test_with(binding) do |result|
        result.should_be_samexml_as("4")
      end
    end
  end
end

context "eval with " do
  include Amrita2::Runtime
  include Amrita2::Filters
  specify "simple" do
    text = <<-END
    <<:people {
        :name => "%s %s" % [$_.first, $_.last]
      }<
      Hello, <<:name>> <br />
    END
    t = Amrita2::Template.new(text)
    People = Struct.new(:first, :last)
    data = {
      :people => [
                   People.new('Yukihiro', 'Matsumoto'),
                   People.new('Taku', 'Nakajima'),
                 ]
    }
    t.test_with(data, binding) do |result|
      result.should_be_samexml_as <<-END
        Hello, Yukihiro Matsumoto <br />
        Hello, Taku Nakajima <br />
      END
    end
  end
  specify "simple with Hash" do
    text = <<-END
    <<:people {
        :name => "%s %s" % [$_[:first], $_[:last]]
      }<
      Hello, <<:name>> <br />
    END
    t = Amrita2::Template.new(text)
    data = {
      :people => {
        :first => 'Taku',
        :last => 'Nakajima',
      }
    }
    t.test_with(data, binding) do |result|
      result.should_be_samexml_as <<-END
        Hello, Taku Nakajima <br />
      END
    end
  end
end
